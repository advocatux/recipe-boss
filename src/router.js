import Vue from 'vue';
import Router from 'vue-router';

import List from '@/views/List';
import Edit from '@/views/Edit';
import View from '@/views/View';

Vue.use(Router);

export default new Router({
    mode: (process.env.VUE_APP_APP_MODE === 'true') ? null : 'history',
    routes: [
        {
            path: '/',
            name: 'list',
            component: List,
        }, {
            path: '/new',
            name: 'new',
            component: Edit,
        }, {
            path: '/edit/:id',
            name: 'edit',
            component: Edit,
        }, {
            path: '/view/:id',
            name: 'view',
            component: View,
        },
    ],
});
