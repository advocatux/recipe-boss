#!/bin/bash

npm run build-app

sed -i 's#=/#=#g' ./dist/index.html
sed -i 's/<link href=vanilla.css rel=stylesheet>/<link href=vanilla.css rel=stylesheet><link href=overrides.css rel=stylesheet>/g' ./dist/index.html

#mkdir -p ./dist/static/css/static/fonts/
#mv ./dist/static/fonts/* ./dist/static/css/static/fonts/
#rmdir ./dist/static/fonts/

rm -rf ./ubuntu-touch-app/www/
mkdir -p ./ubuntu-touch-app/www/
cp -r ./dist/* ./ubuntu-touch-app/www/
